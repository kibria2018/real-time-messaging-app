from flask import Flask, render_template, jsonify
from flask_cors import CORS
from flask_socketio import SocketIO, emit
from models import db, Message
from routes import register_routes
from socketio_events import register_socketio_events

class MessageApp:
    def __init__(self):
        # Create Flask app and configure CORS
        self.app = Flask(__name__)
        CORS(self.app)  # Enable CORS for all routes

        # Configure SQLAlchemy and create the database tables
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///messages.db'
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(self.app)

        # Create the database tables within the application context
        with self.app.app_context():
            try:
                db.create_all()
            except Exception as e:
                print(f"Error creating database tables: {e}")

        # Create SocketIO instance with CORS configuration
        self.socketio = SocketIO(self.app, cors_allowed_origins="*")

        # Register routes and SocketIO events
        register_routes(self.app)
        register_socketio_events(self.socketio)

    def run(self):
        try:
            # Run the application with SocketIO
            self.socketio.run(self.app, debug=True)
        except Exception as e:
            print(f"Error running the application: {e}")
