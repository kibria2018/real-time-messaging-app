from flask_socketio import emit
from models import db, Message

def register_socketio_events(socketio):
    # Define SocketIO event for handling incoming messages
    @socketio.on('send_message')
    def handle_message(data):
        try:
            sender = data.get('sender', '')
            text = data.get('text', '')

            # Save the message to the database
            new_message = Message(sender=sender, text=text)
            db.session.add(new_message)
            db.session.commit()

            # Emit the new message to all connected clients
            emit('new_message', {'sender': sender, 'text': text}, broadcast=True)
        except Exception as e:
            print(f"Error handling message: {e}")
