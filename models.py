# models.py

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender = db.Column(db.String(50))
    text = db.Column(db.Text)

    def __repr__(self):
        return f"{self.sender}: {self.text}"
