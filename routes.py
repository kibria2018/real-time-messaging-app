from flask import jsonify, render_template
from models import Message

def register_routes(app):
    # Define route for the main page
    @app.route('/')
    def index():
        return render_template('index.html')

    # Define route to get messages from the database
    @app.route('/get_messages', methods=['GET'])
    def get_messages():
        try:
            # Retrieve all messages from the database
            messages = Message.query.all()
            messages_list = [{'sender': message.sender, 'text': message.text} for message in messages]
            return jsonify(messages_list)
        except Exception as e:
            return jsonify({'error': f"Error retrieving messages: {e}"}), 500
