# Real-Time Messaging App

This is a simple real-time messaging app built with Flask and SocketIO.

## Features

- Real-time messaging between users.
- Persistent message storage in a SQLite database.
- Basic user interface for a seamless conversation experience.

## Requirements

- Python 3.x
- Flask
- Flask-SocketIO
- Flask-CORS
- Flask-SQLAlchemy

## Installation

1. **Clone the repository:**

   ```bash
   git clone https://github.com/your-username/real-time-messaging-app.git

2. Change into the project directory:

    ```bash
    cd real-time-messaging-app
    ```

3. Create a virtual environment (optional but recommended):

    ```bash
    python -m venv venv
    ```

4. Activate the virtual environment:

    - On Windows:

        ```bash
        venv\Scripts\activate
        ```

    - On macOS and Linux:

        ```bash
        source venv/bin/activate
        ```

5. Install the dependencies:

    ```bash
    pip install -r requirements.txt
    ```

6. Run the project:

    ```bash
    python app.py
    ```

7. Open your web browser and navigate to http://127.0.0.1:5000/ to use the app.

8. Open your web browser and navigate to http://127.0.0.1:5000/get_messages to retrieve all messages from the database.

## Project Structure
- **app.py:** Main application file.
- **message_app.py:** MessageApp class handling the Flask app and SocketIO setup.
- **routes.py:** Routes registration.
- **socketio_events.py:** SocketIO events registration.
- **models.py:** Database models (e.g., Message).
- **static/:** Static files (e.g., CSS, JS).
- **templates/:** HTML templates.
- **venv/:** Virtual environment folder (create and activate as needed).

## Contributing
    Contributions are welcome! Feel free to open issues or pull requests.
    