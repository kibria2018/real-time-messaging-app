// frontend/main.js
const chatContainer = document.getElementById('chat');
const messageInput = document.getElementById('messageInput');
const usernameInput = document.getElementById('usernameInput');

let username;
const socket = io.connect('http://localhost:5000');  // Connect to the Socket.IO server

function renderMessage(message) {
    const div = document.createElement('div');
    div.textContent = message;
    chatContainer.appendChild(div);
}

function setUsername() {
    username = usernameInput.value;
    if (username.trim() !== '') {
        document.getElementById('user-container').style.display = 'none';
        document.getElementById('chat-container').style.display = 'block';
    }
}

function sendMessage() {
    const message = messageInput.value;

    // Send the message to the server using Socket.IO
    socket.emit('send_message', { sender: username, text: message });
    messageInput.value = '';
}

// Listen for new messages and render them
socket.on('new_message', function (data) {
    renderMessage(`${data.sender}: ${data.text}`);
});
