from message_app import MessageApp

if __name__ == '__main__':
    # Create and run the MessageApp
    message_app = MessageApp()
    message_app.run()
